#!/usr/bin/python3
from uuid import uuid4

from swisscom_ais import SwisscomAIS
import logging
logging.basicConfig(filename='swisscom.log', level=logging.DEBUG)

user_input = None

cert_file = 'mycert.crt'
cert_key_file = 'mycert.key'
claimed_identity = "ais-90days-trial:OnDemand-Advanced"
claimed_identity = 'hbl.ch:OnDemand-Advanced'
input_document = "temp.pdf"
signed_document = "temp_signed.pdf"
signature_file = "signature.p7b"
customer_name = "Maxime Monod"
customer_givenname = "Maxime"
customer_surname = "Monod"
customer_location = "Lausanne"
customer_organisation = "Twist Lab"
customer_country = "CH"
customer_email = "maxime.monod@twistlab.ch"
customer_reason = "Digital contract"


ais = SwisscomAIS(cert_file, cert_key_file, claimed_identity)

print("----------------------------------------------------")
print("| TWISTLAB - Swisscom AIS shell                    |")
print("----------------------------------------------------")

responseId = None

while user_input != "q":
    print("[1] Sign a document")
    print("[2] Retrieve a signature and finalize signing")
    print("[q] Quit")
    user_input = input()

    if user_input == '1':
        print("****************************************************")
        print("Calling AIS to get the signature")
        hash_to_sign = ais.pre_sign(input_document, signed_document, customer_name, customer_location, customer_reason)
        print("Hash to sign: " + hash_to_sign)
        print("Please enter your phone number: [i.e. +4179XXXXXXX]")
        phone_number = input()
        response = ais.async_step_up(hash_to_sign, phone_number, customer_name, customer_givenname, customer_surname,
                                     customer_location, customer_organisation, customer_country, customer_email,
                                     "EN", "You are about to sign your digital contract", str(uuid4()))
        print(response)
        responseId = response['SignResponse']['OptionalOutputs']['async.ResponseID']
        print("****************************************************")
    elif user_input == '2':
        print("****************************************************")
        print("Calling AIS to retrieve a signature status and sign the document")
        selected_responseId = None
        if responseId:
            selected_responseId = input("Use last call ResponseID '" + responseId + "' ? [Y/n] ")
            if selected_responseId.lower() == 'y' or selected_responseId == '':
                selected_responseId = responseId
            else:
                selected_responseId = input("Enter ResponseID: ")
        else:
            selected_responseId = input("Enter ResponseID: ")
        response = ais.async_status(selected_responseId)
        try:
            value = response['SignResponse']['SignatureObject']['Base64Signature']['$']
            print("Found a signature object:")
            print(" -> writing signature.p7b")
            open("signature.p7b", "w").write("-----BEGIN PKCS7-----\n" + str(value) + "\n-----END PKCS7-----\n")
            print("    done")
            print("You can check signature validity with: openssl pkcs7 -print_certs -in signature.p7b")
            print("Signing " + signed_document + " with signature")
            ais.sign(signed_document, signature_file)
            print("done")
        except KeyError as e:
            print(response)
        print("****************************************************")

    elif user_input == 'q':
        print("Goodbye")
