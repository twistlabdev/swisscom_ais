.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Swisscom All-in Signing Service, run this command in your terminal:

.. code-block:: console

    $ pip install swisscom_ais

This is the preferred method to install Swisscom All-in Signing Service, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Swisscom All-in Signing Service can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://gitlab.com/twistlabdev/swisscom_ais


Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Gitlab repo: https://gitlab.com/twistlabdev/swisscom_ais
