# -*- coding: utf-8 -*-
"""
Exceptions used throughout the project. All exceptions inherit from SwisscomGenericError
"""


__all__ = ('SwisscomGenericError', 'SwisscomStepUpService', 'SwisscomStepUpStatus',
           'SwisscomStepUpSerialMismatch', 'SwisscomStepUpTimeout')


class SwisscomGenericError(Exception):
    def __init__(self, major=None, minor=None, response_as_json=None, message=''):
        self.major = major
        self.minor = minor
        self.message = message
        self.response_as_json = response_as_json

    def __str__(self):
        return "major: {}, minor: {}, message: {}".format(self.major, self.minor, self.message)


class SwisscomStepUpSerialMismatch(SwisscomGenericError):
    pass


class SwisscomStepUpService(SwisscomGenericError):
    pass


class SwisscomStepUpStatus(SwisscomGenericError):
    pass


class SwisscomStepUpTimeout(SwisscomGenericError):
    pass


class SwisscomStepUpCancel(SwisscomGenericError):
    pass
