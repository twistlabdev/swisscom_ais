# -*- coding: utf-8 -*-

"""Top-level package for Swisscom All-in Signing Service."""

from .swisscom_ais import *  # noqa
from .exceptions import *  # noqa
from .version import *  # noqa
