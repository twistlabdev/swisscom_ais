# -*- coding: utf-8 -*-
import datetime
import json
import logging
import os
import subprocess
import uuid

import requests

from .exceptions import (SwisscomGenericError, SwisscomStepUpService, SwisscomStepUpStatus,
                         SwisscomStepUpSerialMismatch, SwisscomStepUpTimeout)


__all__ = ('SwisscomAIS',)


DEFAULT_DN_FORMAT = 'CN={phone},pseudonym={phone},C=CH,SerialNumber={date:%Y%m%d}-{phone}'
logger = logging.getLogger(__name__)


class SwisscomAIS:
    SIGN_URL = 'https://ais.swisscom.com/AIS-Server/rs/v1.0/sign'
    STATUS_URL = 'https://ais.swisscom.com/AIS-Server/rs/v1.0/pending'

    def __init__(self, cert_file, cert_key_file, claimed_identity, trial_mode=False,
                 distinguished_name_format=DEFAULT_DN_FORMAT):
        """
        Constructor for SwisscomAIS accessor.
        :param cert_file: .crt file
        :param cert_key_file: .key file
        :param claimed_identity: username
        """
        self.cert_file = cert_file
        self.cert_key_file = cert_key_file
        self.claimed_identity = claimed_identity
        self.trial_mode = trial_mode
        self.distinguished_name_format = distinguished_name_format
        self._jar_path = os.path.join(os.path.dirname(__file__))
        self._process = ['java', '-jar', 'ais.jar']

    def async_step_up(self, digest_value, customer_mobile, customer_name, customer_givenname, customer_surname,
                      customer_location, customer_organisation, customer_country, customer_email, language, message,
                      document_id):
        """
        Sign a document using Swisscom AIS (async)

        Production (from a swisscom mail:
        DN-Format:
        cn=<Mobiltelefonnummer des Signierenden mit Präfix "417">
        pseudonym=<Mobiltelefonnummer des Signierenden mit Präfix "417">
        c=ch
        serialNumber=<Aktuelles Datum im Format YYYYMMDD>-<Mobiltelefonnummer des Signierenden mit Präfix "417">
        """
        request_id = str(uuid.uuid4())
        headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
        if self.trial_mode:
            customer_name = 'TEST {}'.format(customer_name)
            customer_organisation = 'TEST {}'.format(customer_organisation)
            if not customer_mobile.startswith('+'):
                customer_mobile = '+' + customer_mobile
        else:
            if customer_mobile.startswith('+'):
                customer_mobile = customer_mobile[1:]
        # dn = "cn={},givenname={},surname={},o={},c={},emailaddress={}".format(
        #    customer_name, customer_givenname, customer_surname,
        #    customer_organisation, customer_country, customer_email
        # )
        dn = self.distinguished_name_format.format(
            name=customer_name,
            given_name=customer_givenname,
            surname=customer_surname,
            location=customer_location,
            organisation=customer_organisation,
            country=customer_country,
            email=customer_email,
            phone=customer_mobile,
            date=datetime.datetime.now(),
        )
        post_json_data = {
            "SignRequest": {
                "@RequestID": request_id,
                "@Profile": "http://ais.swisscom.ch/1.1",
                "OptionalInputs": {
                    "ClaimedIdentity": {"Name": self.claimed_identity},
                    "SignatureType": "urn:ietf:rfc:3369",
                    "sc.AddRevocationInformation": {"@Type": "BOTH"},
                    "AddTimestamp": {"@Type": "urn:ietf:rfc:3161"},
                    "AdditionalProfile": [
                        "http://ais.swisscom.ch/1.0/profiles/ondemandcertificate",
                        "urn:oasis:names:tc:dss:1.0:profiles:asynchronousprocessing",
                        "http://ais.swisscom.ch/1.1/profiles/redirect"
                    ],
                    "sc.CertificateRequest": {
                        "sc.DistinguishedName": dn,
                        "sc.StepUpAuthorisation": {
                            "sc.Phone": {
                                "sc.MSISDN": customer_mobile,
                                "sc.Message": message,
                                "sc.Language": language
                            }
                        }
                    }
                },
                "InputDocuments": {
                    "DocumentHash": [
                        {
                            "@ID": document_id,
                            "dsig.DigestMethod": {"@Algorithm": "http://www.w3.org/2001/04/xmlenc#sha256"},
                            "dsig.DigestValue": digest_value
                        }
                    ]
                }
            }
        }
        try:
            logging.debug(json.dumps(post_json_data))
            response = requests.post(self.SIGN_URL, json=post_json_data, headers=headers,
                                     cert=(self.cert_file, self.cert_key_file))
            if response.status_code == 200:
                return self._check_for_errors(response.json())
            else:
                raise SwisscomStepUpService(
                    'Error while processing request {} with response code: {}'.format(str(response),
                                                                                      str(response.status_code))
                )
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError) as e:
            logger.error('Connection to swisscom service was not made: {}'.format(e))
            raise SwisscomStepUpService() from e

    def async_status(self, response_id):
        """
        Retrieve the status of a signature using Swisscom AIS (asynchronous)
        """
        headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
        post_json_data = {
                "async.PendingRequest": {
                    "@Profile": "http://ais.swisscom.ch/1.1",
                    "OptionalInputs": {
                        "ClaimedIdentity": {"Name": self.claimed_identity},
                        "async.ResponseID": response_id
                    }
                 }
            }
        try:
            response = requests.post(self.STATUS_URL, json=post_json_data, headers=headers,
                                     cert=(self.cert_file, self.cert_key_file))
            if response.status_code == 200:
                return self._check_for_errors(response.json())
            else:
                raise SwisscomStepUpStatus(
                    'Error while processing request {} with response code: {}'.format(str(response),
                                                                                      str(response.status_code))
                )
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError) as e:
            logger.error('Connection to swisscom service was not made: {}'.format(e))
            raise SwisscomStepUpService() from e

    def pre_sign(self, input_pdf_filename, output_pdf_filename, customer_name, customer_location, customer_reason):
        """
        Call the java routine to retrieve the hash and build output pdf with empty signature
        """
        input_path = os.path.abspath(input_pdf_filename)
        output_path = os.path.abspath(output_pdf_filename)
        if self.trial_mode:
            customer_name = 'TEST {}'.format(customer_name)
        try:
            hash_result = subprocess.check_output(
                self._process + [input_path, '-presign', output_path, '"' + customer_name + '"',
                                 '"' + customer_location + '"', '"' + customer_reason + '"'],
                cwd=self._jar_path
            )[:-1].decode("utf-8")
            return hash_result
        except subprocess.CalledProcessError as e:
            raise SwisscomGenericError() from e

    def sign(self, input_pdf_filename, input_p7b_file):
        """
        Call the java routine to sign the pdf given the input p7b signature file
        """
        input_path = os.path.abspath(input_pdf_filename)
        key_path = os.path.abspath(input_p7b_file)
        try:
            completed_process = subprocess.run(self._process + [input_path, '-sign', key_path], cwd=self._jar_path)
        except subprocess.CalledProcessError as e:
            raise SwisscomGenericError() from e
        if completed_process.returncode != 0:
            raise SwisscomGenericError(message='Unable to sign the document. Was it presigned?')

    @staticmethod
    def _check_for_errors(response_as_json):
        try:
            major = response_as_json['SignResponse']['Result']['ResultMajor']
        except KeyError:
            major = ""
        try:
            minor = response_as_json['SignResponse']['Result']['ResultMinor']
        except KeyError:
            minor = ""
        try:
            message = response_as_json['SignResponse']['Result']['ResultMessage']
        except KeyError:
            message = ""

        if major in ('urn:oasis:names:tc:dss:1.0:resultmajor:Success',
                     'urn:oasis:names:tc:dss:1.0:profiles:asynchronousprocessing:resultmajor:Pending'):
            return response_as_json

        if minor == "http://ais.swisscom.ch/1.1/resultminor/subsystem/StepUp/SerialNumberMismatch":
            raise SwisscomStepUpSerialMismatch(major, minor, message, response_as_json)
        elif minor == "http://ais.swisscom.ch/1.1/resultminor/subsystem/StepUp/service":
            raise SwisscomStepUpService(major, minor, message, response_as_json)
        elif minor == "http://ais.swisscom.ch/1.1/resultminor/subsystem/StepUp/status":
            raise SwisscomStepUpStatus(major, minor, message, response_as_json)
        elif minor == "http://ais.swisscom.ch/1.1/resultminor/subsystem/StepUp/timeout":
            raise SwisscomStepUpTimeout(major, minor, message, response_as_json)
        else:
            raise SwisscomGenericError(major, minor, message, response_as_json)
