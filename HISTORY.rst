=======
History
=======


0.4.2 (2019-12-04)
------------------

* if trial mode, append + to the number, if not, remove + to the number. Don't ask me why.


0.4.1 (2019-12-04)
------------------

* Fix

0.4.0 (2019-12-03)
------------------

* Added default DN


0.3.3 (2019-05-15)
------------------

* Fixed issue that added quotes around names


0.3.2 (2019-05-14)
------------------

* Version in specific file to allow installation of dependencies


0.3.1 (2019-05-07)
------------------

* Do not send organisation if empty (non mandatory field)


0.3.0 (2019-05-06)
------------------

* Use trial_mode setting to add TEST to person name
* Log outgoing requests
* Enforce usage of document id


0.2.1 (2019-04-03)
------------------

* Project cleanup


0.2.0 (2019-03-22)
------------------

* Use latest jar from https://gitlab.com/twistlabdev/swisscom_ais_java


0.1.0 (2019-03-18)
------------------

* First release


0.0.0
-----

* original codebase, part of a branch of a Twistlab project
