===============================
Swisscom All-in Signing Service
===============================


.. image:: https://img.shields.io/pypi/v/swisscom_ais.svg
        :target: https://pypi.python.org/pypi/swisscom_ais

.. image:: https://img.shields.io/travis/gfavre/swisscom_ais.svg
        :target: https://travis-ci.org/gfavre/swisscom_ais

.. image:: https://readthedocs.org/projects/swisscom-ais/badge/?version=latest
        :target: https://swisscom-ais.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Python package to sign pdf documents using g Swisscom AIS


* Free software: GNU General Public License v3
* Documentation: https://swisscom-ais.readthedocs.io.


Features
--------


Generating P7B key
------------------

Use openSSL to generate P7B key from crt file::

    openssl crl2pkcs7 -nocrl -certfile mycert.crt -out signature.p7b


Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
