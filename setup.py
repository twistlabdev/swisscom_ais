#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
from distutils.util import convert_path


main_ns = {}
ver_path = convert_path('swisscom_ais/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

with open('requirements/base.txt') as fp:
    install_requires = fp.read().splitlines()
with open('requirements/dev.txt') as fp:
    test_install_requires = fp.read().splitlines()[1:]

setup(
    author="Gregory Favre",
    author_email='dev@twistlab.ch',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="Python package to sign pdf documents using g Swisscom AIS",
    install_requires=install_requires,
    include_package_data=True,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    keywords='swisscom_ais',
    name='swisscom_ais',
    packages=find_packages(include=['swisscom_ais']),

    scripts=['bin/swisscom_ais_sign.py'],
    test_suite='tests',
    tests_require=test_install_requires,
    url='https://gitlab.com/twistlabdev/swisscom_ais',
    version=main_ns['__version__'],
    zip_safe=False,
)
